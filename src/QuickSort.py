#! /bin/puthon3/

def quickSort(A):
   quickSortHelper(A,0,len(A)-1)

def quickSortHelper(A,primero,ultimo):
   if primero<ultimo:

       pivote = particion(A,primero,ultimo)

       quickSortHelper(A,primero,pivote-1)
       quickSortHelper(A,pivote+1,ultimo)


def particion(A,primero,ultimo):
   pivotvalue = A[primero]

   izq = primero+1
   der = ultimo

   done = False
   while not done:

       while izq <= der and A[izq] <= pivotvalue:
           izq = izq + 1

       while A[der] >= pivotvalue and der >= izq:
           der = der -1

       if der < izq:
           done = True
       else:
           temp = A[izq]
           A[izq] = A[der]
           A[der] = temp

   temp = A[primero]
   A[primero] = A[der]
   A[der] = temp


   return der

A = [54,26,93,17,77,31,44,55,20]
quickSort(A)
print(A)
