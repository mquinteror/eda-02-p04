# funcion para crear una lista con sus elementos en 0
def crearLista (tamanio):
    lista = []
    for i in range (tamanio):
        lista.append(0)
    return lista
def valorMaximo (lista): # encuentra el valor maximo de una lista
    maximo =0
    for i in lista:
        if i>=maximo:
            maximo=i
    return maximo
def valorMinimo (lista): # encuentra el valor minimo de una lista
    minimo=0
    for i in lista:
        if i<=minimo:
            minimo=i
    return minimo
    
# funcion que usa el algoritmo de countingsort
def countingSort(lista):
    tamanio = len(lista)
    maximo = valorMaximo(lista)
    sumaocurrencias=0 # suma de las ocurrencias de los valores menores
    #arreglos auxiliares
    listac=crearLista(maximo+1)
    #listab=crearLista(tamanio)
    listab=[]
    # pasos operativos
    for j in lista: # encontrar la cantidad de ocurrencias
        listac[j]=listac[j]+1
    # crear arreglo de suma de ocurrencias
    #    for i in range (0,maximo+1):
    #        listac[i]=listac[i]+sumaocurrencias
    #sumaocurrencias=listac[i]
    # la anterior funcion fue elimindada debido a que no consideramos que fuera mucho de utilidad
    # debilo al siguiene metodo que usamos para ordenar el areglo.
    
    #print(len(listac))
    # En este punto el arreglo ya esta ordenado, solo hay que acabar con la dispersion.
    
    for x in range(0,len(listac)):
        if listac[x]>0:
            # el elemento no esta disperso
            ocurrencias = listac[x]
            while ocurrencias > 0:
                listab.append(x)
                ocurrencias=ocurrencias-1
    return listab

# pruebas
arreglo1=(1,3,6,1,10)
arreglo1=(1,2,4,5,8,10,4,5,2,6,12,15,12,16,50)
arreglo2=(15,14,13,12,11,10,9,8,7,6,5,5,5,5,5)
arreglo3=(0,1,10,0,20,83,8,82,8,6,2,4,5,6,5)
print('Arreglos originales')
print(arreglo1)
print(arreglo2)
print(arreglo3)
print('Arreglos Ordenados')
resultado = countingSort(arreglo1)
print(countingSort(arreglo1))
#print(resultado)
print(countingSort(arreglo2))
print(countingSort(arreglo3))

